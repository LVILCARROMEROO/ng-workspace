import {Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {validExtension} from '@wkmg/commons/utils';

@Component({
  selector: 'app-file-form',
  templateUrl: './file-form.component.html',
  styleUrls: ['./file-form.component.scss']
})
export class FileFormComponent {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<FileFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { validExtensions: string[] }
  ) {
    this.form = this.fb.group({
      file: ['', [Validators.required, validExtension(this.data.validExtensions)]],
    });
  }

  get fileField() {
    return this.form.get('file');
  }

  save() {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    }
  }

  onFileChange(event) {
    // form data
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.form.patchValue({
        file
      });

      /* base 64
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        // reader.result --> base64 string
      };
      */
    }
  }
}
