import { Component, OnInit } from '@angular/core';
import {Label} from 'ng2-charts';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {WorkshopsService} from '@wkmg/commons/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = ['2016', '2017'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  // public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];

  constructor(private workshopsService: WorkshopsService) { }

  ngOnInit() {
    this.workshopsService.getAll()
      .subscribe(
        workshops => {
          this.barChartLabels = workshops.map(workshop => workshop.name);

          const chartsData = {};
          workshops.forEach((workshop, index) => {
            if (!(workshop.instructor._id in chartsData)) {
              const data = Array.from(new Array(workshops.length).keys()).map(key => 0);
              chartsData[workshop.instructor._id] = { data: [...data], label: workshop.instructor.lastNames };
            }

            chartsData[workshop.instructor._id].data[index] = workshop.participants;
          });

          this.barChartData = Object.values(chartsData);
        },
        err => console.log(err)
      );
  }

}
